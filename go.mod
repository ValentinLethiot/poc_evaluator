module bitbucket.org/ValentinLethiot/poc_evaluator

go 1.12

require (
	github.com/antlr/antlr4 v0.0.0-20200309161749-1284814c2112 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/nikunjy/rules v0.0.0-20200120082459-0b7c4dc9dc86
	github.com/stretchr/testify v1.5.1
)
