package main_test

import (
	"testing"

	"github.com/nikunjy/rules/parser"
	"github.com/stretchr/testify/assert"
)

func TestParser(t *testing.T) {
	//Given
	rule := "x eq 1"
	obj := make(map[string]interface{}, 0)
	obj["x"] = 1

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserAnd(t *testing.T) {
	//Given
	rule := "x eq 1 and y eq \"bonjour\""
	obj := make(map[string]interface{}, 0)
	obj["x"] = 1
	obj["y"] = "bonjour"

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserOr(t *testing.T) {
	//Given
	rule := `x eq 9 or y eq "bonjour"`
	obj := make(map[string]interface{}, 0)
	obj["x"] = 1
	obj["y"] = "bonjour"

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserIn(t *testing.T) {
	//Given
	rule := "x in [1, 2, 3]"
	obj := make(map[string]interface{}, 0)
	obj["x"] = 1

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserNotIn(t *testing.T) {
	//Given
	rule := "not(x in [1, 2, 3])"
	obj := make(map[string]interface{}, 0)
	obj["x"] = 5

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserNotFound(t *testing.T) {
	//Given
	rule := "x in [1, 2, 3]"
	obj := make(map[string]interface{}, 0)
	obj["y"] = 1

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, false, "Resp should be false")
}

func TestParserNotEqual(t *testing.T) {
	//Given
	rule := "x ne true"
	obj := make(map[string]interface{}, 0)
	obj["x"] = false

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserLessThan(t *testing.T) {
	//Given
	rule := "x lt 2"
	obj := make(map[string]interface{}, 0)
	obj["x"] = 1

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserLessThanOrEqual(t *testing.T) {
	//Given
	rule := "x le 2"
	obj := make(map[string]interface{}, 0)
	obj["x"] = 2

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserLessThanWithString(t *testing.T) {
	//Given
	rule := "x le 2"
	obj := make(map[string]interface{}, 0)
	obj["x"] = "a"

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, false, "Resp should be false")
}

func TestParserGreaterThan(t *testing.T) {
	//Given
	rule := "x gt 2"
	obj := make(map[string]interface{}, 0)
	obj["x"] = 4

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserGreaterThanOrEqual(t *testing.T) {
	//Given
	rule := "x ge 2"
	obj := make(map[string]interface{}, 0)
	obj["x"] = 2

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserNot(t *testing.T) {
	//Given
	rule := "x eq 1 and not (y eq 2)"
	obj := make(map[string]interface{}, 0)
	obj["x"] = 1
	obj["y"] = 3

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserContains(t *testing.T) {
	//Given
	rule := `email co "@symbol-it.fr"`
	obj := make(map[string]interface{}, 0)
	obj["email"] = "valentin.lethiot@symbol-it.fr"

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserNotContains(t *testing.T) {
	//Given
	rule := `not(email co "@symbol-it.fr")`
	obj := make(map[string]interface{}, 0)
	obj["email"] = "valentin.lethiot@symbol-it.fr"

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, false, "Resp should be false")
}

func TestParserStartWith(t *testing.T) {
	//Given
	rule := `email sw "valentin.lethiot"`
	obj := make(map[string]interface{}, 0)
	obj["email"] = "valentin.lethiot@symbol-it.fr"

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserNotStartWith(t *testing.T) {
	//Given
	rule := `not(email sw "valentin.lethiot")`
	obj := make(map[string]interface{}, 0)
	obj["email"] = "valentin.lethiot@symbol-it.fr"

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, false, "Resp should be false")
}

func TestParserEndsWith(t *testing.T) {
	//Given
	rule := `email ew ".fr"`
	obj := make(map[string]interface{}, 0)
	obj["email"] = "valentin.lethiot@symbol-it.fr"

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserNotEndsWith(t *testing.T) {
	//Given
	rule := `not(email ew ".com")`
	obj := make(map[string]interface{}, 0)
	obj["email"] = "valentin.lethiot@symbol-it.fr"

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserPresent(t *testing.T) {
	//Given
	rule := `x pr`
	obj := make(map[string]interface{}, 0)
	obj["x"] = true

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}

func TestParserNotPresent(t *testing.T) {
	//Given
	rule := `x pr`
	obj := make(map[string]interface{}, 0)
	obj["y"] = true

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, false, "Resp should be false")
}

func TestBigEvaluate(t *testing.T) {
	//Given
	obj := make(map[string]interface{}, 0)
	obj["email"] = "valentin.lethiot@symbol-it.fr"
	obj["pays"] = "France"
	rule := `(email pr) and 
			 (email sw "valentin") and
			 (email co "@symbol-it") and
			 (email ew ".fr") and
			 (pays pr) and
			 (pays eq "France" or pays eq "Belgique")`

	//When
	resp := parser.Evaluate(rule, obj)

	//Then
	assert.Equal(t, resp, true, "Resp should be true")
}